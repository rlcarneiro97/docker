# Prática1 #

<!-- TOC -->

- [Prática1](#prática1)
- [Máquina Virtual](#máquina-virtual)
- [Google Cloud](#google-cloud)
- [Playground Docker](#playground-docker)
- [Docker-CE](#docker-ce)
- [Docker-Compose](#docker-compose)

<!-- TOC -->

# Máquina Virtual

Se estiver usando uma máquina virtual instalada e configurada conforme as instruções previamente enviadas pelo professor, ligue a máquina virtual e passe para a seção [Docker-CE](#docker-ce).


# Google Cloud

Se estiver usando o Google Cloud para praticar os exercícios, crie uma instância de máquina virtual no Google Cloud com as especificações abaixo. Orientação mais precisas serão passadas pelo professor.

* Label: curso-docker
* region: us-centra1 (Iowa)
* Series: N1
* Machine type: n1-standard-1
* CPU: 1 vCPU;
* Memória: 3.75 GB;
* HD: 10 GB;
* Sistema Operacional: Ubuntu 20.04 LTS minimal;
* Firewall
  * Allow HTTP traffic
  * Allow HTTPS traffic
  * Networking > Networking tag: curso-docker

Acesse a instância via SSH (a partir do console do navegador web) e passe para a seção [Docker-CE](#docker-ce). Orientação mais precisas serão passadas pelo professor.

# Playground Docker

Se estiver usando o site [Play with Docker](https://labs.play-with-docker.com) para praticar os exercícios, faça login com a gratuita, previamente crida no [Docker Hub](https://hub.docker.com), e passe para a seção [Docker-Compose](#docker-compose).

# Docker-CE

Instale o Docker-CE seguindo as instruções das páginas a seguir, de acordo com o sistema operacional.

* No Ubuntu:
https://docs.docker.com/install/linux/docker-ce/ubuntu/

* No CentOS:
https://docs.docker.com/install/linux/docker-ce/centos/

* No Debian:
https://docs.docker.com/install/linux/docker-ce/debian/

Inicie o serviço Docker, configure o Docker para ser inicializado no boot do S.O e adicione o seu usuário ao grupo Docker.

```bash
# Inicie o serviço Docker
sudo systemctl start docker

# Configure o Docker para ser inicializado no boot do S.O
sudo systemctl enable docker

# Adicione o seu usuário ao grupo Docker
sudo usermod -aG docker $USER
sudo apt install acl
sudo setfacl -m user:$USER:rw /var/run/docker.sock
```

Fonte: https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot

# Docker-Compose

Instale o Docker Compose com os seguintes comandos.

```bash
COMPOSE_VERSION=1.26.0

sudo curl -L "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

/usr/local/bin/docker-compose version
```

Maiores informações podem ser obtidas na documentação oficial: https://docs.docker.com/compose/install/
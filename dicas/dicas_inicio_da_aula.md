<!-- TOC -->

- [Dicas para executar no início de cada aula](#dicas-para-executar-no-início-de-cada-aula)
- [Google Cloud](#google-cloud)

<!-- TOC -->

# Dicas para executar no início de cada aula

# Google Cloud

* Ligue as instâncias a serem utilizadas na aula.

Procedimento para ligar as instâncias:

1) No Google Cloud (https://console.cloud.google.com), clique no menu **Compute engine** e escolha a opção **VM instances**.
2) Selecione as instâncias utilizadas na aula e clique no botão **START**.
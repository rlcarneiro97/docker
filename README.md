# curso-docker #

<!-- TOC -->

- [curso-docker](#curso-docker)
- [Sobre](#sobre)
  - [Requisitos](#requisitos)
  - [Instruções](#instruções)
  - [Desenvolvedor](#desenvolvedor)
  - [Licença](#licença)

<!-- TOC -->

# Sobre

Este é o repositório de execícios referentes ao curso sobre Docker, ministrado por Aécio Pires.

Para obter mais informações sobre o curso acesse o link abaixo:

http://blog.aeciopires.com/curso-docker/

## Requisitos

1. Use uma distribuição GNU/Linux ou crie uma conta gratuita no Google Cloud (http://cloud.google.com) ou baixe uma máquina virtual indicada pelo professor ou crie uma conta gratuita no Docker Hub (https://hub.docker.com), que será usada para acessar o site **Play with Docker**: https://labs.play-with-docker.com.
2. Para criar uma conta no Google Cloud você precisa cadastrar um cartão de crédito e associar à conta criada no Google Cloud. Você receberá US$ 300 para utilizar qualquer serviço da Google Cloud durante o período de teste de 365 dias. Você só será cobrado após expirar o período de teste ou se gastar todos os créditos antes desse período. **Esse créditos iniciais são mais que suficientes para a execução do curso**.

## Instruções

* Baixe os códigos com os seguintes comandos ou acesse a página https://gitlab.com/cursos_/docker/tags e o obtenha o pacote referente a tag mais recente.

```bash
git clone https://gitlab.com/cursos_/docker.git
cd docker
```

* Execute os exercícios na ordem citada ao longo do curso.

## Desenvolvedor

Aécio dos Santos Pires
Site: http://aeciopires.com

## Licença

Copyright (c) 2020 Aécio dos Santos Pires

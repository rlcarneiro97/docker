<!-- TOC -->

- [Objetivos](#objetivos)
- [Usando volumes a partir de um diretório do Docker Host.](#usando-volumes-a-partir-de-um-diretório-do-docker-host)
- [Criando volumes portáveis.](#criando-volumes-portáveis)

<!-- TOC -->

# Objetivos

O objetivo deste exercício é mostrar como gerenciar volumes no Docker Host e mapeá-los com contêineres para evitar a perda de dados quando os mesmos forem removidos.

São apresentadas duas formas de gerenciar volumes no Docker:

A primeira é criando manualmente o diretório no Docker Host e mapeando para um diretório específico dentro do contêiner, no qual uma aplicação deve ser configurada para salvar os dados. Assim quando a aplicação escrever os dados no diretório especificado, automaticamente os dados estarão sendo escritos no Docker Host. Isso facilita o backup dos dados.

A segunda é criando volumes portáveis. No volume portável, vocẽ não especifica onde o diretório será criado no Docker Host, isso é gerenciado pelo serviço Docker. Mas você consegue mapear o volume portável para um diretório específico dentro do contêiner para a aplicação escrever os dados. A vantagem disso é que você garante que os volumes serão criados no mesmo padrão em qualquer Docker Host.

Você também verá que é possível usar uma mesma imagem Docker para criar contêineres diferentes para disponibilizar o mesmo serviço para clientes diferentes.

# Usando volumes a partir de um diretório do Docker Host.

Crie os diretórios ``/opt/docker/site1`` e ``/opt/docker/site2``.

```bash
sudo mkdir -p /opt/docker/site1
sudo mkdir -p /opt/docker/site2
```

Inicie o contêiner do Tomcat.

```bash
docker run -d --rm -p 8888:8080 --name=tomcat tomcat:9-jdk11-openjdk
docker pull tomcat:9-jdk11-openjdk

```

Acesse o Tomcat no navegador pela URL http://IP-SERVIDOR:8888.

Pare o Tomcat.

```bash
docker stop tomcat
```

Verifique se o contêiner foi removido. Se sim, qual o motivo disso?

```bash
docker ps
docker ps -a
```

Copie o diretório ``ROOT``, existente no repositório para os diretórios criados no início do exercício.

```bash
cd /tmp
git clone https://gitlab.com/cursos_/docker.git
cd docker
sudo cp -R pratica4/ROOT /opt/docker/site1/
sudo cp -R pratica4/ROOT /opt/docker/site2/
```

Inicie o contêiner do Tomcat mapeando um volume a ser criado no diretório anterior do Docker Host e montando em uma pasta específica do contêiner.

```bash
docker run -d --rm -p 8888:8080 --name=tomcat -v /opt/docker/site1/ROOT:/usr/local/tomcat/webapps/ROOT tomcat:9-jdk11-openjdk
```

Acesse novamente o Tomcat no navegador pela URL http://IP-SERVIDOR:8888 e verifique se algo mudou.
Se sim, você acaba de fazer um deploy da aplicação exemplo no Tomcat, sobrescrevendo a aplicação padrão pela existente em um volume do Docker Host. Essa aplicação será persistida quando o contêiner for removido e poderá ser usado em outro contêiner.

Inicie outro contêiner do Tomcat para disponibilizar o site 2.

```bash
docker run -d --rm -p 8889:8080 --name=tomcat2 -v /opt/docker/site2/ROOT:/usr/local/tomcat/webapps/ROOT tomcat:9-jdk11-openjdk
```

Abra outra aba do navegador e acesse a URL: http://IP-SERVIDOR:8889/

É a mesma aplicação?

Pare os contêiners do Tomcat.

```bash
docker stop tomcat
docker stop tomcat2
```

Verifique se a aplicação exemplo ainda está nos diretórios ``/opt/docker/site1`` e ``/opt/docker/site2``.

> Ao montar um volume manualmente é necessário passar o caminho completo do diretório de origem no Docker Host e de destino no contêiner. Se tentar passar um caminho relativo, você verá um erro semelhante a este: ``docker: Error response from daemon: create pratica4/ROOT: "pratica4/ROOT" includes invalid characters for a local volume name, only "[a-zA-Z0-9][a-zA-Z0-9_.-]" are allowed. If you intended to pass a host directory, use absolute path.``

# Criando volumes portáveis.

Crie o volume ``nginx_data``.

```bash
docker volume create nginx_data
```

Liste os volumes existentes no Docker Host.

```bash
docker volume ls | more
```

Inspecione o volume ``nginx_data`` e verifique em qual diretório do Docker Host, ele está montado.

```bash
docker volume inspect nginx_data | grep Mountpoint
```

Copie os arquivos do diretório ``nginx_config``, para dentro do diretório do volume ``nginx_data``. No comando a seguir, substitua o termo ``NAME_DIR`` pelo diretório exibido no resultado do comando anterior.

```bash
sudo cp /tmp/docker/pratica4/nginx_config/* NAME_DIR/
```

Inicie um contêiner do Nginx.

```bash
docker run --name nginx -v nginx_data:/etc/nginx/ -p 443:443 -p 80:80 -d nginx
```

> Se estiver executando os exercícios no GNU/Linux instalado diretamente no computador/notebook, execute os seguintes comandos:

```bash
sudo su
echo "127.0.0.1 docker.curso.info" >> /etc/hosts
exit
```

> Se estiver executando os exercícios em uma máquina virtual, execute os seguintes comandos no seu computador/notebook (onde ``IP_MAQUINA_VIRTUAL`` deve ser substituído pelo IP da máquina virtual):

```bash
sudo su
echo "IP_MAQUINA_VIRTUAL docker.curso.info" >> /etc/hosts
exit
```

> Se estiver executando os exercícios em uma instância da cloud, execute os seguintes comandos no seu computador/notebook (onde ``IP_PUBLIC_INSTANCE_CLOUD`` deve ser substituído pelo IP público da instância):

```bash
sudo su
echo "IP_PUBLIC_INSTANCE_CLOUD docker.curso.info" >> /etc/hosts
exit
```

Acesse o Nginx na URL https://IP-SERVIDOR.

> Se tudo tiver ocorrido com sucesso, você receberá um erro 404 na tela, mas verá que foi configurado um certificado para uso do HTTPS para o host ``docker.curso.info``. Este certificado é o que foi copiado para dentro do volume do ``nginx``.

Pare o contêiner e remova do nginx.

```bash
docker stop nginx
docker rm nginx
```

Verifique se ainda existe os arquivos de configuração e certificados no diretório usado pelo volume ``nginx_data``.

Remova o volume ``nginx_data``.

```bash
docker volume rm nginx_data
```

Verifique se ainda existe os arquivos de configuração e certificados no diretório usado pelo volume removido.

<!-- TOC -->

- [Objetivos](#objetivos)
- [Gerenciando Redes no Docker Host](#gerenciando-redes-no-docker-host)

<!-- TOC -->

# Objetivos

O objetivo deste exercício é mostrar como gerenciar redes no Docker Host e associá-las aos contêineres

# Gerenciando Redes no Docker Host

Liste as redes Docker existentes no Docker Host.

```bash
docker network ls
```

Inspecione a rede ``bridge``.

```bash
docker network inspect bridge
```

Inicie um conteiner com o Ubuntu.

```bash
docker run --name=ubuntu -i -t -d ubuntu:20.04 /bin/bash
```

Inspecione novamente a rede ``bridge`` e veja qual é o IP associado ao conteiner do Ubuntu.

```bash
docker network inspect bridge
```

Crie uma rede chamada ``minha_rede_docker``, usando o driver ``bridge``.

```bash
docker network create --driver bridge minha_rede_docker
```

Inspecione a rede ``minha_rede_docker``.

```bash
docker network inspect minha_rede_docker
```

Pare o conteiner do ubuntu e crie-o novamente associando-o à rede criada anteriormente.

```bash
docker stop ubuntu
docker rm ubuntu
docker run --name=ubuntu --network=minha_rede_docker -i -t -d ubuntu:20.04 /bin/bash
```

Inspecione novamente a rede ``bridge`` e veja qual é o IP associado ao conteiner do Ubuntu.

```bash
docker network inspect minha_rede_docker
```

Pare o conteiner do ubuntu e remova a rede criada anteriormente.

```bash
docker stop ubuntu
docker rm ubuntu
docker network rm minha_rede_docker
```

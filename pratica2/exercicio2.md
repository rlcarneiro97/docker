<!-- TOC -->

- [Objetivos](#objetivos)
- [Exercícios](#exercícios)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como obter a documentação do comandos, subcomandos e opções do Docker;
* Apresentar alguns comandos mais usados do Docker;
* Mostrar a diferença entre uma imagem e um contêiner;
* Mostrar como resolver problemas de conflitos de porta e nome de contêiner;
* Refoçar a ideia de que um contêiner é descartável e que os dados podem ser perdidos, se não for armazenados em um volume Docker (assunto a ser apresentado mais adiante).
* Mostrar que é possível usar uma mesma imagem Docker para criar contêineres diferentes para disponibilizar o mesmo serviço para clientes diferentes no mesmo host.

# Exercícios

Baixe uma imagem do Grafana customizada pelo professor.

```bash
docker pull aeciopires/ubuntu-grafana:v7
```

Inspecione a imagem baixada recentemente.

```bash
docker image inspect aeciopires/ubuntu-grafana:v7
```

Inicie um contêiner interativo.

```bash
docker run -i -t -p 3000:3000 --name=ctn1 aeciopires/ubuntu-grafana:v7 /bin/bash
```

Acesse o Grafana através do navegador com os dados abaixo.

URL: http://IP-SERVIDOR:3000
Login: admin
Senha: admin

Em outro terminal de comandos no seu Docker Host, liste os contêineres em execução.

```bash
docker ps
```

Visualize os recursos usados pelo contêiner.

```bash
docker stats ctn1
```

Liste as portas usadas pelo contêiner ``ctn1``.

```bash
docker port ctn1
```

Tente explicar como o Grafana pode ser acessado no navegador, se o mesmo está sendo executado em um contêiner.

Ainda no outro terminal de comandos, inicie outro contêiner interativo do Grafana.

```bash
docker run -i -t -p 3000:3000 --name=ctn2 aeciopires/ubuntu-grafana:v7 /bin/bash
```

O que aconteceu e por que?

Pare o contêiner ``ctn1``.

```bash
docker stop ctn1
```

Inicie outro contêiner interativo do Grafana.

```bash
docker run -i -t -p 3000:3000 --name=ctn3 aeciopires/ubuntu-grafana:v7 /bin/bash
```

O que aconteceu e por que?

Inicie o contêiner ``ctn1``.

```bash
docker start ctn1
```

Ainda no outro terminal de comandos, inicie uma sessão shell com o contêiner ``ctn1`` (como se estivesse abrindo uma sessão via SSH no contêiner) e instale o ``tcpdump``.

```bash
docker exec -i -t ctn1 /bin/bash
apt-get update
apt-get install -y tcpdump
```

Save as imagens do seu Docker Host para um arquivo a ser carregado em outro Docker Host. Essa é uma alternativa para copiar imagens de um computador para outro sem precisar de acesso à Internet para usar o Docker Hub.

```bash
docker save -o /tmp/imagens_a_compartilhar.tar ubuntu:20.04 aeciopires/ubuntu-grafana:v7
```

Remova as imagens existentes.

```bash
docker rmi ubuntu:20.04 aeciopires/ubuntu-grafana:v7
```

Carregue as imagens a partir do arquivo exportado.

```bash
docker load -i /tmp/imagens_a_compartilhar.tar
```

Liste as imagens existentes no Docker Host e inspecione a imagem do Ubuntu para verificar se as camadas foram carregadas corretamente.

```bash
docker images
docker image inspect ubuntu:20.04
```

Pare os contêineres ``ctn1``, ``ctn2`` e ``ctn3``

```bash
docker stop ctn1 ctn2 ctn3
```

Remova os contêineres ``ctn1``, ``ctn2`` e ``ctn3``

```bash
docker rm ctn1 ctn2 ctn3
```

Verifique se ainda tem algum contêiner parado.

```bash
docker ps -a
```

Remova todos os contêineres parados.

```bash
docker rm $(docker ps -a -q)
```

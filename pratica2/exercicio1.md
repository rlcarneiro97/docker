<!-- TOC -->

- [Objetivos](#objetivos)
- [Exercícios](#exercícios)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como obter a documentação do comandos, subcomandos e opções do Docker;
* Apresentar alguns comandos mais usados do Docker;
* Mostrar a diferença entre uma imagem e um contêiner;
* Mostrar como resolver problemas de conflitos de porta e nome de contêiner;
* Refoçar a ideia de que um contêiner é descartável e que os dados podem ser perdidos, se não for armazenados em um volume Docker (assunto a ser apresentado mais adiante).
* Mostrar que é possível usar uma mesma imagem Docker para criar contêineres diferentes para disponibilizar o mesmo serviço para clientes diferentes no mesmo host.

# Exercícios

Considerando que você já tem o pacote ``docker-ce`` instalado no seu Docker Host, obtenha a ajuda do comando ``docker``.

```bash
docker help
docker rm --help
docker run --help
```

Obtenha as informações sobre o seu Docker Host (máquina física ou virtual na qual são executados os contêineres).

```bash
docker info
```

Pesquise no Docker Hub (será mostrado mais adiante) por repositórios com imagens do Ubuntu.

```bash
docker search ubuntu
```

Acesse a página https://hub.docker.com/search/ pela imagem ``ubuntu`` e compare o resultados com o do comando anterior.

Viu a diferença entre uma imagem oficial e uma não oficial?

Acesse a página https://hub.docker.com/_/ubuntu/ e observe as informações desse repositório.

Tem informações sobre como baixar a imagem? Viu como obter a lista de tags associadas a imagem?

Baixe a imagem Docker do Ubuntu 20.04 a partir do repositório oficial no Docker Hub.

```bash
docker pull ubuntu:20.04
```

Liste as imagens já obtidas no seu Docker Host.

```bash
docker images
```

Visualize o histórico de mudanças na imagem.

```bash
docker history ubuntu:20.04
```

Inspecione (faça uma auditoria) na imagem.

```bash
docker inspect ubuntu:20.04
```

Obtenha a ajuda do comando ``docker run``.

```bash
docker run --help
```

Execute um contêiner interativo a partir da imagem do Ubuntu e instale o pacote ``iputils-ping``.

```bash
docker run -i -t ubuntu:20.04 /bin/bash
apt-get update
apt-get install -y iputils-ping
ping google.com
```

Abra outro terminal de comandos no seu Docker Host e liste os contêineres em execução.

```bash
docker ps
```

Viu o ID, nome e imagem usado pelo contêiner?

Inspecione o contêiner em execução, trocando o termo ``ID_CONTAINER`` pelo ID do contêiner apresentado na primeira coluna da primeira linha do comando anterior.

```bash
docker inspect ID_CONTAINER
```

Viu alguma diferença entre a inspeção de uma imagem e uma inspeção de um contêiner?

Liste os processos em execução de um contêiner.

```bash
docker top ID_CONTAINER
```

Digite os seguintes comandos para visualizar determinados processos em execução no Docker Host.

```bash
sudo su
ps aux | grep ubuntu
ps aux | grep ping
```

Percebeu que o contêiner é executado como um processo do Docker Host?

Percebeu que é possível visualizar um processo executado no contêiner a partir do Docker Host? É possível fazer isso se em uma máquina virtual?

Encerre o processo do ping executado no contêiner com o seguinte comando trocando o termo PID pelo número apresentado na segunda coluna no resultado do comando ``ps aux | grep ping``.

```bash
ps aux | grep ping
kill -9 PID
```

Volte para o terminal no qual está sendo executado o contêiner e instale o pacote ``nmap``.

```bash
apt-get install -y nmap
nmap --version
```

Agora saia do contêiner.

```bash
exit
```

Liste os contêineres em execução.

```bash
docker ps
```

Liste os contêineres parados.

```bash
docker ps -a
```

O contêiner parou? Se sim, por que?

Execute novamente o contêiner interativo a partir da mesma imagem do Ubuntu.

```bash
docker run -i -t ubuntu:20.04 /bin/bash
```

Dentro do contêiner, verifique se o ``nmap`` está instalado.

```bash
nmap --version
```

O ``nmap`` estava instalado? O que acha que aconteceu?

Agora saia do contêiner.

```bash
exit
```

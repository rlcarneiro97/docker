<!-- TOC -->

- [Informações sobre a imagem do My_Ubuntu no DockerHub](#informações-sobre-a-imagem-do-my_ubuntu-no-dockerhub)
  - [Como usar a imagem?](#como-usar-a-imagem)
  - [Compile a imagem Docker a partir do DockerFile.](#compile-a-imagem-docker-a-partir-do-dockerfile)
  - [Referências:](#referências)

<!-- TOC -->

# Informações sobre a imagem do My_Ubuntu no DockerHub

A imagem está disponível na URL a seguir.

https://hub.docker.com/r/aeciopires/my_ubuntu/

O My_Ubuntu é o Ubuntu customizado a partir da imagem base da versão 20.04.

A imagem providencia os seguintes pacotes.

* Java
* Vim
* Git
* Wget

## Como usar a imagem?

* Baixe a imagem do DockerHub:

```sh
docker pull aeciopires/my_ubuntu:20.04
```

* Crie um contêiner com uma das formas abaixo.

a) contêiner em background:

```sh
docker run -d --name=my_ubuntu aeciopires/my_ubuntu:20.04
```

b) contêiner interativo:

```sh
docker run -i -t --name=my_ubuntu aeciopires/my_ubuntu:20.04 /bin/bash
```

## Compile a imagem Docker a partir do DockerFile.

* Baixe o código do repositório Git.

```sh
git clone https://gitlab.com/cursos_/docker
```

```sh
cd docker/pratica9/my_ubuntu/
docker build -t aeciopires/my_ubuntu:20.04 .
```

## Referências:

https://docs.docker.com/engine/tutorials/dockerimages/

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04

https://docs.docker.com/engine/reference/commandline/commit/

https://www.digitalocean.com/community/tutorials/docker-explained-how-to-create-docker-containers-running-memcached

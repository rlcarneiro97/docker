<!-- TOC -->

- [Instruções para baixar e compilar as imagens Docker do JHipster](#instruções-para-baixar-e-compilar-as-imagens-docker-do-jhipster)
  - [Obtendo o código fonte](#obtendo-o-código-fonte)
  - [Configurações prévias a compilação das imagens](#configurações-prévias-a-compilação-das-imagens)
  - [Requisitos para executar os contêineres](#requisitos-para-executar-os-contêineres)

<!-- TOC -->

# Instruções para baixar e compilar as imagens Docker do JHipster

## Obtendo o código fonte

Baixe o código do repositório Git com o comando a seguir.

```bash
git clone https://gitlab.com/cursos_/docker
cd docker/pratica9/jhipster_myapp
```

## Configurações prévias a compilação das imagens

* Quando a equipe de desenvolvimento gerar uma nova versão de um
microsserviço, é necessário que o arquivo ``Dockerfile``, relacionado ao
serviço, seja alterado para dar suporte as novas dependências de
software ou configuração. Neste mesmo arquivo, também deve ser alterada
a **variável que contém versão**. Exemplo:

Antes:

```bash
ARG GATEWAY_VERSION="1.0.0"
```

Depois:

```bash
ARG GATEWAY_VERSION="1.1.0"
```


* Antes de compilar as imagens dos microsserviços, edite os arquivos
``application.yml`` e/ou ``bootstrap.yml`` de cada microsserviço e
ajuste os parâmetros de acordo com o ambiente.

* Para cada microsserviço funcionar, é necessário que o Registry esteja em execução.

## Requisitos para executar os contêineres

* No ambiente de desenvolvimento, instale o MySQL com o comando a seguir.

```bash
docker run -d --name mysql -p 3306:3306 --restart always -e MYSQL_ROOT_PASSWORD=root_mysql -e MYSQL_DATABASE=gateway -e MYSQL_USER=jhipster -e MYSQL_PASSWORD=jhipster mysql:5
```

* É necessário a criação do seguinte banco de dados.

Configuração dos bancos de dados a serem criados.

| Serviço    | Banco            | Usuário          | Senha            |
| ---------- | ---------------- | ---------------- | ---------------- |
| Gateway    | gateway       | jhipster       | jhipster       |

* Siga as instruções [daqui](docker_compose/README.md) para subir todos os contêineres do Jhipster com o Docker Compose.

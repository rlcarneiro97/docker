<!-- TOC -->

- [Instruções para baixar e compilar a imagem Docker](#instruções-para-baixar-e-compilar-a-imagem-docker)
  - [Configurações prévias](#configurações-prévias)
  - [Compile a imagem Docker.](#compile-a-imagem-docker)
  - [Inicie o contêiner.](#inicie-o-contêiner)
  - [Visualize o log do microserviço.](#visualize-o-log-do-microserviço)
  - [Se precisar remover o contêiner, use o comando abaixo.](#se-precisar-remover-o-contêiner-use-o-comando-abaixo)

<!-- TOC -->

# Instruções para baixar e compilar a imagem Docker

## Configurações prévias

Realize as configurações prévias citadas [aqui](../README.md).

## Compile a imagem Docker.

Considerando que você já realizou as configurações prévias, use o
comando a seguir para gerar uma imagem do JHipster-Gateway para uma versão.

```bash
cd jhipster-myapp/gateway
docker build  -t jhipster_myapp/gateway:0.0.1 .
```

## Inicie o contêiner.

Use o comando a seguir para iniciar um contêiner do JHipster-Gateway.

```bash
docker run -d -p 8080:8080 --name gateway -e APP_PROFILE="prod" jhipster_myapp/gateway:0.0.1
```

## Visualize o log do microserviço.

```bash
docker logs -f gateway
```

## Se precisar remover o contêiner, use o comando abaixo.

```bash
docker rm -f gateway
```

Acesse o JHipster-Gateway na URl http://localhost:8080 ou http://IP-DOCKER_HOST:8080

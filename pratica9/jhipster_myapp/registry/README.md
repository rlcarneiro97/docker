<!-- TOC -->

- [Instruções para baixar e compilar a imagem Docker](#instruções-para-baixar-e-compilar-a-imagem-docker)
  - [Configurações prévias](#configurações-prévias)
  - [Compile a imagem Docker.](#compile-a-imagem-docker)
  - [Inicie o contêiner.](#inicie-o-contêiner)
  - [Visualize o log do microserviço.](#visualize-o-log-do-microserviço)
  - [Se precisar remover o contêiner, use o comando abaixo.](#se-precisar-remover-o-contêiner-use-o-comando-abaixo)
  - [Acesso ao Registry](#acesso-ao-registry)

<!-- TOC -->

# Instruções para baixar e compilar a imagem Docker

## Configurações prévias

Realize as configurações prévias citadas [aqui](../README.md).

## Compile a imagem Docker.

1. Considerando que você já realizou as configurações prévias, use o
comando a seguir para gerar uma imagem do Registry para uma versão.

```bash
cd docker/pratica9/jhipster_myapp/registry
docker build  -t jhipster-myapp/registry:3.2.3 .
```

## Inicie o contêiner.

Use o comando a seguir para iniciar um contêiner do Registry.

```bash
docker run -d -p 8761:8761 --name registry -e APP_PROFILE="prod" jhipster-myapp/registry:3.2.3
```

## Visualize o log do microserviço.

```bash
docker logs -f registry
```


## Se precisar remover o contêiner, use o comando abaixo.

```bash
docker rm -f registry
```

## Acesso ao Registry

Acesse o microserviço através da URL http://localhost:8761 ou http://IP-DOCKER-HOST:8761.

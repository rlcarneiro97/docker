<!-- TOC -->

- [Instruções para baixar e compilar a imagem Docker](#instruções-para-baixar-e-compilar-a-imagem-docker)
  - [Configurações prévias](#configurações-prévias)
- [Instruções para baixar e iniciar o Docker Compose](#instruções-para-baixar-e-iniciar-o-docker-compose)
  - [Inicie os conteiners](#inicie-os-conteiners)
  - [Acesso ao Gateway](#acesso-ao-gateway)
  - [Pare os conteiners](#pare-os-conteiners)

<!-- TOC -->

# Instruções para baixar e compilar a imagem Docker

## Configurações prévias

Realize as configurações prévias citadas [aqui](../README.md).

# Instruções para baixar e iniciar o Docker Compose

Para instalar o Docker Compose, use os comandos a seguir.

```bash
COMPOSE_VERSION=1.26.0

sudo curl -L "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

/usr/local/bin/docker-compose version
```

## Inicie os conteiners

Inicie os microserviços com o comando abaixo.

```bash
cd docker/pratica9/jhipster_myapp/docker_compose/
docker-compose -f docker-compose_jhipster.yml up --build
```

## Acesso ao Gateway

Acesse a interface do Gateway através da URL http://localhost:8080 ou http://IP-DOCKER_HOST:8080.

Acesse a interface do Registry através da URL http://localhost:8761 ou http://IP-DOCKER_HOST:8761.

## Pare os conteiners

Use os comandos a seguir para parar os conteineres.

```bash
cd docker/pratica9/jhipster_myapp/docker_compose/
docker-compose down
```

Para obter mais informações sobre o Docker Compose acesse:

* [https://docs.docker.com/compose/reference/](https://docs.docker.com/compose/reference/)
* [https://docs.docker.com/compose/compose-file/](https://docs.docker.com/compose/compose-file/)

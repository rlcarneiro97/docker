<!-- TOC -->

- [Instruções](#instruções)

<!-- TOC -->

# Instruções

Observe e analise os arquivos necessários para compilar as imagens customizadas das aplicações abaixo.

* my_ubuntu
* jhispter-registry
* jhipster-gateway

Observe e analise as instruções para compilar cada imagem.

Observe e analise as instruções para iniciar os serviços.

Com base em todas as informações aprendidas em sala e nos exemplos dados, elas serão úteis para a realização do trabalho final.


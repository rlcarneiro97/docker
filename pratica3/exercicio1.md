<!-- TOC -->

- [Objetivos](#objetivos)
- [Trabalhando com Conteineres](#trabalhando-com-conteineres)

<!-- TOC -->

# Objetivos

O objetivo deste exercício é mostrar como obter imagens e usá-las para criar contêineres para fornecer um serviço. Você verá como enviar uma imagem para o Docker Hub.

# Trabalhando com Conteineres

Inicie um contêiner do MySQL para ser executado em background e crie o banco de dados para ser usado pelo wordpress.

```bash
docker run -d --name mysql-wordpress -p 3306:3306 --restart always -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress mysql:5
```

Observe que o contêiner ``mysql-wordpress`` está usando a imagem ``mysql:5``. Como ela não tinha sido baixada antes, o Docker automaticamente baixa antes de iniciar o contêiner.

> Em ambientes de produção evite utilizar imagens Docker com a tag ``latest``, que geralmente aponta para a versão mais nova. A versão mais nova da imagem pode trazer alguma quebra de compatibilidade que pode provocar a interrupção do serviço. A boa prática é associar a uma tag específica na hora de baixar uma imagem Docker. Exemplo: use ``ubuntu:18:04`` ao invés de ``ubuntu`` ou ``ubuntu:latest`` (que aponta atualmente para o Ubuntu 20.04). O Ubuntu 20.04 pode não ser uma versão homologada para uso em produção no seu ambiente.

Inicie um contêiner do Wordpress para ser executado em background.

```bash
docker run --name wordpress -d -p 80:80  --restart always -e WORDPRESS_DB_HOST="172.17.0.1" -e WORDPRESS_DB_USER="wordpress" -e WORDPRESS_DB_PASSWORD="wordpress" -e WORDPRESS_DB_NAME="wordpress" wordpress:php7.4-apache
```

Visualize os logs dos contêineres iniciados anteriormente.

```bash
docker logs mysql-wordpress
docker logs wordpress
```

Acesse o Wordpress na URL http://IP-SERVIDOR. Configure o Wordpress.

Acesse o terminal de comandos do Docker Host, em seguida, pare e remova os contêineres criados anteriormente.

```bash
docker stop wordpress
docker stop mysql-wordpress
docker rm wordpress mysql-wordpress
```

Inicie os contêineres novamente e verifique se o Wordpress está instalado e tente explicar o que aconteceu.

```bash
docker run -d --name mysql-wordpress -p 3306:3306 --restart always -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress mysql:5

docker run --name wordpress -d -p 80:80  --restart always -e WORDPRESS_DB_HOST="172.17.0.1" -e WORDPRESS_DB_USER="wordpress" -e WORDPRESS_DB_PASSWORD="wordpress" -e WORDPRESS_DB_NAME="wordpress" wordpress:php7.4-apache
```

Pare e remova os contêineres criados anteriormente.

```bash
docker stop wordpress
docker stop mysql-wordpress
docker rm wordpress mysql-wordpress
```

Liste as imagens já obtidas no Docker Host.

```bash
docker images
```

Remova a imagem do Wordpress.

```bash
docker rmi wordpress
```

Inicie um contêiner do Grafana novamente em background.

```bash
docker run -d -p 3000:3000 --name=ctn1 aeciopires/ubuntu-grafana:v7
```

Acesse o terminal do contêiner ``ctn1`` (como se estivesse abrindo uma sessão via SSH no contêiner) e instale o ``telnet``.

```bash
docker exec -i -t ctn1 /bin/bash
apt-get update
apt-get install -y telnet
exit
```

Crie uma conta no Docker Hub e crie um repositório para hospedar a imagem customizada a ser gerada a partir do contêiner ``ctn1``.

Crie um commit a partir da imagem do contêiner ``ctn1`` para persistir a nova camada adicionada na imagem do Grafana, após a instalação do ``telnet``.

```bash
docker commit -m "MENSAGEM_COMMIT" ctn1 CONTA_DOCKER_HUB/NOME_REPOSITORIO:TAG_VERSAO
```

Visualize as imagens locais.

```bash
docker images
```

O que aconteceu? Por que?

Faça login no Docker Hub.

```bash
docker login -u CONTA_DOCKER_HUB
```

Envie a nova imagem para o Docker Hub.

```bash
docker push CONTA_DOCKER_HUB/NOME_REPOSITORIO:TAG_VERSAO
```

> Não há uma regra para determinar o nome da tag. Cada time tem a liberdade de usar o nome que quiser. Uma boa prática é usar o [semantic version](https://semver.org) ou associar a tag de um commit. Exemplo: https://hub.docker.com/r/jupyter/scipy-notebook/tags

Inicie um contêiner do RabbitMQ.

```bash
docker run -d --name rabbitmq \
 -p 5672:5672 \
 -p 15672:15672 \
 --restart=always \
 --hostname rabbitmq-master \
 rabbitmq:3.8-management
```

Visualize o log do RabbitMQ.

```bash
docker logs -f rabbitmq
```

Acesse o RabbitMQ na URL http://IP-SERVIDOR:15672. O login é ``guest`` e a senha padrão é ``guest``.

Visualize os recursos de CPU e memória utilizados pelo contêiner.

```bash
docker stats rabbitmq
```

Remova o contêiner do RabbitMQ.

```bash
docker rm -f rabbitmq
```

> O comando ``docker rm -f`` remove o contêiner sem parar o serviço. Isso pode ser um problema em produção caso a aplicação esteja escrevendo/lendo algo do banco de dados ou do disco.

Inicie o contêiner do RabbitMQ novamente limitando o uso de memória pelo contêiner a 2GB.

```bash
docker run -d --name rabbitmq \
 -p 5672:5672 \
 -p 15672:15672 \
 --restart=always \
 --hostname rabbitmq-master \
 -m 400m \
 rabbitmq:3.8-management
```

Visualize os recursos de CPU e memória utilizados pelo contêiner.

```bash
docker stats rabbitmq
```

Pare e remova o contêiner do RabbitMQ.

```bash
docker rm -f rabbitmq
```

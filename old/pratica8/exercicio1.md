Crie um diretório para armazenar as imagens no Docker host.

```bash
sudo mkdir -p /opt/docker/registry/data
```

Crie um diretório para armazenar o certificado a ser usado pelo Docker Registry.

```bash
sudo mkdir -p /opt/docker/registry/certs
```

Copie os arquivos do certificado que estão no diretório ``pratica6``.

```bash
cd /tmp
git clone https://gitlab.com/cursos_/docker
cd docker
sudo cp pratica8/certs/*  /opt/docker/registry/certs
```

Cadastre o nome ``docker.curso.info`` no arquivo ``/etc/hosts`` (esse nome de host foi usado na geração do certificado). Cadastre também o certificado auto-assinado no Ubuntu, a ser usado pelo Docker Registry.

```bash
sudo su
echo "172.17.0.1 docker.curso.info" >> /etc/hosts
cp pratica8/certs/server.crt /usr/local/share/ca-certificates
update-ca-certificates
service docker restart
```

Inicie um conteiner para o Docker Registry.

```bash
docker run -d -p 5000:5000 --add-host docker.curso.info:172.17.0.1 --restart=always --name registry -v /opt/docker/registry/certs:/certs -v /opt/docker/registry/data:/var/lib/registry -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/server.crt -e REGISTRY_HTTP_TLS_KEY=/certs/server.key registry:2
```

Baixe a versão mais nova da imagem do PostgreSQL do Docker Hub

```bash
docker pull postgres:latest
```

Cadastre a imagem obtida do PostgreSQL no Docker Registry local com a tag v10.0.

```bash
docker tag postgres:latest docker.curso.info:5000/postgres:v10.0
docker push docker.curso.info:5000/postgres:v10.0
```

Agora visualize as imagens existentes no Docker Host.

```bash
docker images
```

Agora remova todas as imagens do PostgreSQL.

```bash
docker rmi postgres:latest docker.curso.info:5000/postgres:v10.0
```

Verifique se o conteiner do registry está em execução.

```bash
docker ps
```

Verifique se a imagem cadastrada no Docker Registry está no volume persistido.

```bash
ls -lR /opt/docker/registry/data
```

Agora baixe a imagem do PostgreSQL usando o Docker Registry ao invés do Docker Hub (registry padrão).

```bash
docker pull docker.curso.info:5000/postgres:v10.0
```

Agora visualize as imagens existentes no Docker Host.

```bash
docker images
```

O download aconteceu mais rápido?

Reinicie novamente o serviço Docker.

```bash
sudo service docker restart
```

Será que o conteiner do Registry parou? O que aconteceu e por que?

Inicie um conteiner com o PostgreSQL usando a imagem obtida do Docker Registry.

```bash
docker run -d --name=ctn4 docker.curso.info:5000/postgres:v10.0
```

Liste os repositórios criados no Docker Registry.

```bash
curl --insecure -i https://docker.curso.info:5000/v2/_catalog
```

A documentação da API do Docker Registry está disponível em:
[https://github.com/docker/distribution/blob/master/docs/spec/api.md](https://github.com/docker/distribution/blob/master/docs/spec/api.md)

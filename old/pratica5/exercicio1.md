Visualize os IPs do Docker Host (máquina física ou virtual na qual são executados os conteineres).

```bash
ifconfig
```

Inicie o Docker Swarm. Lembre-se de trocar IP-ADDRESS no comando abaixo pelo IP do Docker Host.

```bash
docker swarm init --advertise-addr IP-ADDRESS
```

O host atual será o primeiro ``node manager`` do cluster Docker Swarm.

Se precisar adicionar mais um host como nó manager, execute o comando abaixo no primeiro node do cluster e copie o resultado para ser executado no segundo host a ser adicionado como um node manager.

```bash
docker swarm join-token manager
```

Se precisar adicionar mais um host como o nó worker, execute o comando abaixo no primeiro node do cluster e copie o resultado para ser executado no segundo host a ser adicionado como um node worker.

```bash
docker swarm join-token worker
```

Visualize todos os nodes cadastrados no cluster.

```bash
docker node ls
```

Crie um conteiner Jenkins como serviço em apenas um dos nodes do cluster. Crie também um volume para persistir os dados no cluster.

```bash
docker volume create jenkins_data
docker service create -p 8080:8080 -p 50000:50000 --hostname jenkins --host jenkins:127.0.0.1 --detach=false --name=jenkins  --mount type=volume,source=jenkins_data,target=/var/jenkins_home jenkins/jenkins:lts
```

Em cada node do cluster, inspecione o volume ``jenkins_data``.

```bash
docker volume inspect jenkins_data
```

Olhe o conteúdo do diretório no qual o volume está montado.

Inspecione o serviço Jenkins.

```bash
docker service inspect jenkins
```

Liste os serviços em execução no cluster.

```bash
docker service ls
```

Aumente o número de replicas do serviço Jenkins para 2.

```bash
docker service scale jenkins=2 --detach=false
```

Liste os serviços em execução no cluster.

```bash
docker service ls
```

o que aconteceu de diferente?

Inspecione novamente o serviço Jenkins.

```bash
docker service inspect jenkins
```

Visualize os logs do serviço jenkins.

```bash
docker service logs jenkins
```

Acesse o Jenkins no navegador usando a URL http://IP-ADDRESS:8080 (lembre-se de trocar IP-ADDRESS pelo IP do host) e continue a instalação.

Fonte: [https://github.com/jenkinsci/docker/blob/master/README.md](https://github.com/jenkinsci/docker/blob/master/README.md)

Reinicie o serviço Docker.

```bash
sudo service docker restart
```

Liste os serviços em execução no cluster.

```bash
docker service ls
```

O serviço Jenkins ainda está funcionando após o reinício do serviço Docker?
Acesse o navegador e olhe se houve ou não perda de sessão na interface Web do Jenkins.

Remova o serviço Jenkins.

```bash
docker service rm jenkins
```

Remova o volume ``jenkins_data``.

```bash
docker volume rm jenkins_data
```

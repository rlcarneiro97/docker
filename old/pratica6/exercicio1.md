Considerando que você já criou o cluster Docker Swarm, execute os comandos abaixo para criar um conteiner como serviço para o Portainer e persistir os dados no diretório ``/opt/docker/portainer/data`` do Docker Host.

```bash
sudo mkdir -p /opt/docker/portainer/data

docker pull portainer/portainer

docker service create --name portainer --detach=false --publish 9000:9000 --constraint 'node.role == manager' --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock --mount type=bind,src=/opt/docker/portainer/data,dst=/data  portainer/portainer   -H unix:///var/run/docker.sock
```

O Portainer ficará acessível na URL http://localhost:9000 ou http://IP-CLUSTER-DOCKER:9000.
